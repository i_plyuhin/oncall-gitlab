SRE indicators implementation
======
#### Minikube Kubernetes cluster with availability and reliability monitoring for Oncall [ https://github.com/linkedin/oncall ] application
### [SLO Specification](/SLO_document.md)
<p align="center"><img src="./docs/source/_static/cluster_scheme.png"></p>
<p align="center"> Cluster architecture</p>


Setup
-----------------
### Prerequisites
Install kvm
```bash
# Проверить включена ли виртуализация
grep -E -c "vmx|svm" /proc/cpuinfo
# Если результат команды > 0, Виртуализация доступна и включена, иначе включить в биосе

# Устанавить KVM и зависимости
sudo apt update
sudo apt install -y qemu qemu-kvm libvirt-daemon bridge-utils virt-manager virtinst

# Проверить, что демон управления виртуализацией libvirtd запущен
systemctl status libvirtd.service

# Добавить пользователя в группы
sudo usermod -aG libvirt $USER
sudo usermod -aG kvm $USER

# Перезагрузиться
sudo reboot
```
Install minikube 
```bash
#Устанавливаем minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

### Run

```bash
. start_minikube.sh
```
<p align="center"><img src="./docs/source/_static/dashboard1.png"></p>
<p align="center"><img src="./docs/source/_static/dashboard2.png"></p>

