#!/bin/bash

for i in `seq 1 5`; do
    mysql -h $MYSQL_HOST -u root --password=$MYSQL_ROOT_PASSWORD -e 'show databases;' && break
    echo "[*] Waiting for mysql to start..."
    sleep 5
done

echo "[*] Loading MySQL schema..."
mysql -h $MYSQL_HOST -u root --password=$MYSQL_ROOT_PASSWORD < ./db/schema.v0.sql
echo "[*] Loading MySQL dummy data..."
mysql -h $MYSQL_HOST -u root --password=$MYSQL_ROOT_PASSWORD -o oncall < ./db/dummy_data.sql

echo "[*] Tables created for database oncall:"
mysql -h $MYSQL_HOST -u root --password=$MYSQL_ROOT_PASSWORD -o oncall -e 'show tables;'
