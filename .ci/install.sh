#!/bin/bash

apt-get update
apt-get install -y libsasl2-dev  libldap2-dev libssl-dev default-mysql-client
python setup.py develop
pip install -e .[dev]

bash .ci/setup_mysql.sh
