import posixpath
import random
import string
import sys
import logging
import requests
import signal
import time
import datetime
from environs import Env
import prometheus_client
from prometheus_client import start_http_server, Gauge, Counter

prometheus_client.REGISTRY.unregister(prometheus_client.GC_COLLECTOR)
prometheus_client.REGISTRY.unregister(prometheus_client.PLATFORM_COLLECTOR)
prometheus_client.REGISTRY.unregister(prometheus_client.PROCESS_COLLECTOR)

PROBER_CREATE_USER_SCENARIO_TOTAL = Counter(
    "oncall_prober_create_user_scenario_total", "Total count of runs the create user scenario to oncall API"
)
PROBER_CREATE_USER_SCENARIO_SUCCESS_TOTAL = Counter(
    "oncall_prober_create_user_scenario_success_total",
    "Total count of successful runs the create user scenario to oncall API"
)
PROBER_CREATE_USER_SCENARIO_FAIL_TOTAL = Counter(
    "oncall_prober_create_user_scenario_failed_total", "Total count of failed runs the create user scenario to oncall API"
)
PROBER_CREATE_USER_SCENARIO_DURATION_SECONDS = Gauge(
    "oncall_prober_create_user_scenario_duration_seconds",
    "Duration in seconds of runs the create user scenario to oncall API"
)

PROBER_DELETE_USER_SCENARIO_TOTAL = Counter(
    "oncall_prober_delete_user_scenario_total", "Total count of runs the delete user scenario to oncall API"
)
PROBER_DELETE_USER_SCENARIO_SUCCESS_TOTAL = Counter(
    "oncall_prober_delete_user_scenario_success_total",
    "Total count of successful runs the delete user scenario to oncall API"
)
PROBER_DELETE_USER_SCENARIO_FAIL_TOTAL = Counter(
    "oncall_prober_delete_user_scenario_failed_total", "Total count of failed runs the delete user scenario to oncall API"
)
PROBER_DELETE_USER_SCENARIO_DURATION_SECONDS = Gauge(
    "oncall_prober_delete_user_scenario_duration_seconds",
    "Duration in seconds of runs the delete user scenario to oncall API"
)

PROBER_CREATE_TEAM_SCENARIO_TOTAL = Counter(
    "oncall_prober_create_team_scenario_total", "Total count of runs the create team scenario to oncall API"
)
PROBER_CREATE_TEAM_SCENARIO_SUCCESS_TOTAL = Counter(
    "oncall_prober_create_team_scenario_success_total", "Total count of successful runs the create team scenario to oncall API"
)
PROBER_CREATE_TEAM_SCENARIO_FAIL_TOTAL = Counter(
    "oncall_prober_create_team_scenario_failed_total", "Total count of failed runs the create team scenario to oncall API"
)
PROBER_CREATE_TEAM_SCENARIO_DURATION_SECONDS = Gauge(
    "oncall_prober_create_team_scenario_duration_seconds", "Duration in seconds of runs the create team scenario to oncall API"
)

PROBER_DELETE_TEAM_SCENARIO_TOTAL = Counter(
    "oncall_prober_delete_team_scenario_total", "Total count of runs the delete team scenario to oncall API"
)
PROBER_DELETE_TEAM_SCENARIO_SUCCESS_TOTAL = Counter(
    "oncall_prober_delete_team_scenario_success_total", "Total count of successful runs the delete team scenario to oncall API"
)
PROBER_DELETE_TEAM_SCENARIO_FAIL_TOTAL = Counter(
    "oncall_prober_delete_team_scenario_failed_total", "Total count of failed runs the delete team scenario to oncall API"
)
PROBER_DELETE_TEAM_SCENARIO_DURATION_SECONDS = Gauge(
    "oncall_prober_delete_team_scenario_duration_seconds", "Duration in seconds of runs the delete team scenario to oncall API"
)

PROBER_CREATE_EVENT_SCENARIO_TOTAL = Counter(
    "oncall_prober_create_event_scenario_total", "Total count of runs the create event scenario to oncall API"
)
PROBER_CREATE_EVENT_SCENARIO_SUCCESS_TOTAL = Counter(
    "oncall_prober_create_event_scenario_success_total", "Total count of successful runs the create event scenario to oncall API"
)
PROBER_CREATE_EVENT_SCENARIO_FAIL_TOTAL = Counter(
    "oncall_prober_create_event_scenario_failed_total", "Total count of failed runs the create event scenario to oncall API"
)
PROBER_CREATE_EVENT_SCENARIO_DURATION_SECONDS = Gauge(
    "oncall_prober_create_event_scenario_duration_seconds", "Duration in seconds of runs the create event scenario to oncall API"
)

env = Env()
env.read_env()


class Config(object):
    oncall_prober_base_url = env("ONCALL_PROBER_BASE_URL", "http://oncall.com")
    oncall_prober_api_url = env("ONCALL_PROBER_API_URL", posixpath.join(oncall_prober_base_url, "api/v0"))
    oncall_prober_scrape_interval = env.int("ONCALL_PROBER_SCRAPE_INTERVAL", 30)
    oncall_prober_log_level = env.log_level("ONCALL_PROBER_LOG_LEVEL", logging.DEBUG)
    oncall_prober_port = env.int("ONCALL_PROBER_PORT", 9300)


class Probe:
    def __init__(self, total_counter, success_counter, fail_counter, duration_seconds):
        self.total_counter = total_counter
        self.success_counter = success_counter
        self.fail_counter = fail_counter
        self.duration_seconds = duration_seconds

    def run(self) -> bool:
        raise Exception("NotImplementedException")


class CreateUserProbe(Probe):
    def __init__(self, total_counter: Counter, success_counter: Counter, fail_counter: Counter, duration_seconds: Gauge,
                 username: str, oncall_api_url: str) -> None:
        self.username = username
        self.oncall_api_url = oncall_api_url
        super().__init__(total_counter, success_counter, fail_counter, duration_seconds)

    def run(self) -> bool:
        response = requests.post(f"{self.oncall_api_url}/users", json={"name": self.username})
        return response and response.status_code == 201


class DeleteUserProbe(Probe):
    def __init__(self, total_counter: Counter, success_counter: Counter, fail_counter: Counter, duration_seconds: Gauge,
                 username: str, oncall_api_url: str) -> None:
        self.username = username
        self.oncall_api_url = oncall_api_url
        super().__init__(total_counter, success_counter, fail_counter, duration_seconds)

    def run(self) -> bool:
        response = requests.delete(f"{self.oncall_api_url}/users/{self.username}")
        return response and response.status_code == 200


class CreateTeamProbe(Probe):
    def __init__(self, total_counter: Counter, success_counter: Counter, fail_counter: Counter, duration_seconds: Gauge,
                 username: str, team: str, oncall_base_url: str, oncall_api_url: str) -> None:
        self.username = username
        self.team = team
        self.oncall_api_url = oncall_api_url
        self.oncall_base_url = oncall_base_url
        super().__init__(total_counter, success_counter, fail_counter, duration_seconds)

    def run(self) -> bool:
        session = requests.session()
        resp = session.post(f"{self.oncall_base_url}/login", data={
            "username": self.username,
            "password": self.username
        })

        csrf = resp.json()["csrf_token"]

        response = session.post(f"{self.oncall_api_url}/teams", json={
            "name": self.team,
            "scheduling_timezone": "EU/Moscow",
        }, headers={"X-CSRF-TOKEN": csrf}, timeout=10)

        return response and response.status_code == 201


class DeleteTeamProbe(Probe):
    def __init__(self, total_counter: Counter, success_counter: Counter, fail_counter: Counter, duration_seconds: Gauge,
                 username: str, team: str, oncall_api_url: str) -> None:
        self.username = username
        self.team = team
        self.oncall_api_url = oncall_api_url
        super().__init__(total_counter, success_counter, fail_counter, duration_seconds)

    def run(self) -> bool:
        response = requests.delete(f"{self.oncall_api_url}/teams/{self.team}")
        return response and response.status_code == 200


class CreateEventProbe(Probe):
    def __init__(self, total_counter: Counter, success_counter: Counter, fail_counter: Counter, duration_seconds: Gauge,
                 username: str, team: str, oncall_api_url: str, start_time: int, end_time: int, role: str) -> None:
        self.username = username
        self.team = team
        self.oncall_api_url = oncall_api_url
        self.start_time = start_time
        self.end_time = end_time
        self.role = role
        super().__init__(total_counter, success_counter, fail_counter, duration_seconds)

    def run(self) -> bool:
        resp = requests.post(f"{self.oncall_api_url}/events", json={
            "start": self.start_time,
            "end": self.end_time,
            "user": self.username,
            "role": self.role,
            "team": self.team
        })
        return resp and resp.status_code == 201


class OncallProberClient:
    def __init__(self, config: Config) -> None:
        self.oncall_api_url = config.oncall_prober_api_url
        self.oncall_base_url = config.oncall_prober_base_url
        self.probes = None
        self.init_probes()

    def init_probes(self):
        salt = "".join(random.choices(string.ascii_lowercase + string.digits, k=10))
        username = 'test_prober_user_' + salt
        team = 'test_prober_team_' + salt
        self.probes = [
            CreateUserProbe(
                total_counter=PROBER_CREATE_USER_SCENARIO_TOTAL,
                success_counter=PROBER_CREATE_USER_SCENARIO_SUCCESS_TOTAL,
                fail_counter=PROBER_CREATE_USER_SCENARIO_FAIL_TOTAL,
                duration_seconds=PROBER_CREATE_USER_SCENARIO_DURATION_SECONDS,
                username=username,
                oncall_api_url=self.oncall_api_url
            ),
            CreateTeamProbe(
                total_counter=PROBER_CREATE_TEAM_SCENARIO_TOTAL,
                success_counter=PROBER_CREATE_TEAM_SCENARIO_SUCCESS_TOTAL,
                fail_counter=PROBER_CREATE_TEAM_SCENARIO_FAIL_TOTAL,
                duration_seconds=PROBER_CREATE_TEAM_SCENARIO_DURATION_SECONDS,
                username=username,
                team=team,
                oncall_base_url=self.oncall_base_url,
                oncall_api_url=self.oncall_api_url
            ),
            CreateEventProbe(
                total_counter=PROBER_CREATE_EVENT_SCENARIO_TOTAL,
                success_counter=PROBER_CREATE_EVENT_SCENARIO_SUCCESS_TOTAL,
                fail_counter=PROBER_CREATE_EVENT_SCENARIO_FAIL_TOTAL,
                duration_seconds=PROBER_CREATE_EVENT_SCENARIO_DURATION_SECONDS,
                username=username,
                team=team,
                oncall_api_url=self.oncall_api_url,
                start_time=int(datetime.datetime.now().timestamp()),
                end_time=int((datetime.datetime.now() + datetime.timedelta(days=1)).timestamp()),
                role="primary"
            ),
            DeleteTeamProbe(
                total_counter=PROBER_DELETE_TEAM_SCENARIO_TOTAL,
                success_counter=PROBER_DELETE_TEAM_SCENARIO_SUCCESS_TOTAL,
                fail_counter=PROBER_DELETE_TEAM_SCENARIO_FAIL_TOTAL,
                duration_seconds=PROBER_DELETE_TEAM_SCENARIO_DURATION_SECONDS,
                username=username,
                team=team,
                oncall_api_url=self.oncall_api_url
            ),
            DeleteUserProbe(
                total_counter=PROBER_DELETE_USER_SCENARIO_TOTAL,
                success_counter=PROBER_DELETE_USER_SCENARIO_SUCCESS_TOTAL,
                fail_counter=PROBER_DELETE_USER_SCENARIO_FAIL_TOTAL,
                duration_seconds=PROBER_DELETE_USER_SCENARIO_DURATION_SECONDS,
                username=username,
                oncall_api_url=self.oncall_api_url
            ),
        ]

    def probe(self) -> None:
        for probe in self.probes:
            probe.total_counter.inc()
            logging.debug(f"Run '{probe.total_counter}' scenario for '{probe.username}'")

            start = time.perf_counter()
            is_success = False
            try:
                is_success = probe.run()
            except Exception as err:
                logging.error(err)
                probe.fail_counter.inc()

            if is_success:
                probe.success_counter.inc()
                logging.debug("Successful")
            else:
                probe.fail_counter.inc()
                logging.debug("Failed")

            duration = (time.perf_counter() - start)
            probe.duration_seconds.set(duration)


def setup_logging(config: Config):
    logging.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%H:%M:%S',
                        level=config.oncall_prober_log_level)
    logging.getLogger("urllib3").setLevel(logging.WARNING)


def main():
    config = Config()
    setup_logging(config)

    logging.info(
        f"Starting prober on port: {config.oncall_prober_port}")
    start_http_server(config.oncall_prober_port)
    client = OncallProberClient(config)

    while True:
        logging.debug(f"Run prober")
        client.init_probes()
        client.probe()

        logging.debug(
            f"Waiting {config.oncall_prober_scrape_interval} seconds for next loop")
        time.sleep(config.oncall_prober_scrape_interval)


def terminate(signal, frame):
    print("Terminating")
    sys.exit(0)


if __name__ == "__main__":
    signal.signal(signal.SIGTERM, terminate)
    main()
