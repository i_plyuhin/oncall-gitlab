local grafana = import 'grafonnet/grafana.libsonnet';
local dashboard = grafana.dashboard;
local template = grafana.template;
local row = grafana.row;
local textPanel = grafana.text;
local graphPanel = grafana.graphPanel;
local statPanel = grafana.statPanel;
local prometheus = grafana.prometheus;
local sql = grafana.sql;

local mysql_src = 'MySQL';
local prometheus_src = 'Prometheus';
local table_name = 'indicators';

// Panels
local totalSla = textPanel.new(
    title='Total SLA',
    content='Total SLA',
    mode='markdown',
    description='fafafa',
);

local slaSpecification = textPanel.new(
    title='SLA specification',
    content='PRIVET PRIVET',
    mode='markdown',
    description='fafafa',
);

local sliPercentage(title, query) = statPanel.new(
    title=title,
    datasource=mysql_src,
    unit='percentunit',
    colorMode='value',
    thresholdsMode='absolute'
  ).addTargets([
    sql.target(
      rawSql=query,
      format='table'
    ),
  ])
  .addThresholds([
    { color: 'text', value: null },
    { color: 'dark-red', value: 0.85 },
    { color: 'yellow', value: 0.9 },
    { color: 'green', value: 0.95 },
]);

local stateTimeline(title) = {
      datasource: mysql_src,
      targets: [
        {
          format: "table",
          refId: 'A',
          rawSql: "SELECT datetime as time, value FROM indicators
                        WHERE node = $node
                        and name = 'oncall_prober_create_user_scenario_fail_total'
                        and datetime between NOW() - interval 1 hour and NOW()"
        },
      ],
      title: title,
      type: 'state-timeline',
      options: {
        "alignValue": "left",
        "legend": {
          "displayMode": "list",
          "placement": "bottom",
          "showLegend": true
        },
        "mergeValues": false,
        "rowHeight": 0.9,
        "showValue": "never",
        "tooltip": {
          "mode": "single",
          "sort": "none"
        }
      },
      "mappings": [
            {
              "options": {
                "0": {
                  "color": "green",
                  "index": 0,
                  "text": "success"
                }
              },
              "type": "value"
            },
            {
              "options": {
                "from": 1,
                "result": {
                  "color": "dark-red",
                  "index": 1,
                  "text": "fail"
                },
                "to": 1000
              },
              "type": "range"
            }
          ]
};


dashboard.new(
  title='SLA',
  editable=true,
  refresh='1m',
)
.addTemplate(
  template.new(
    name='scenario',
    datasource=mysql_src,
    query='SELECT name FROM indicators;',
    allValues=null,
    current='all',
    refresh='load',
    includeAll=true,
    multi=true,
    regex='/^(.*?scenario.*?)/',
  )
)
.addTemplate(
  template.new(
    name='node',
    datasource=prometheus_src,
    query='label_values(kube_pod_info{namespace="oncall"}, node)',
    allValues=null,
    current='minikube',
    refresh='load',
    includeAll=true,
    multi=false,
  )
)
.addPanels([
  row.new(
    title='Main',
    showTitle=true,
  ) + { gridPos: { h: 2, w: 24, x: 0, y: 0 } },

  totalSla + { gridPos: { h: 6, w: 6, x: 0, y: 0 } },
  slaSpecification + { gridPos: { h: 6, w: 18, x: 6, y: 0 } },

  row.new(
    title='$scenario',
    repeat='scenario',
    showTitle=true,
  ) + { gridPos: { h: 2, w: 24, x: 0, y: 6 } },

  sliPercentage(title="SLI: $scenario success", query="SELECT (60 - COUNT(*)) / 60 FROM indicators
                                    WHERE node = $node
                                    and name = 'oncall_prober_create_user_scenario_fail_total'
                                    and datetime between NOW() - interval 1 hour and NOW()
                                    and is_bad") + { gridPos: { h: 6, w: 6, x: 0, y: 6 } },


  stateTimeline(title="SLI: $scenario success") + { gridPos: { h: 6, w: 6, x: 6, y: 6 }},

  sliPercentage(title="SLI: $scenario duration seconds", query="SELECT (60 - COUNT(*)) / 60 FROM indicators
                                    WHERE node = $node
                                    and name = 'oncall_prober_create_user_scenario_duration_seconds'
                                    and datetime between NOW() - interval 1 hour and NOW()
                                    and is_bad") + { gridPos: { h: 6, w: 6, x: 12, y: 6 } },


])