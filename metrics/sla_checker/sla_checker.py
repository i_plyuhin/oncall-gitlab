import sys
import logging
import requests
import signal
import time
import math
from datetime import datetime

from environs import Env
import mysql.connector

env = Env()
env.read_env()


class Config(object):
    prometheus_api_url = env("PROMETHEUS_API_URL", 'http://prom.com')
    scrape_interval = env.int("SCRAPE_INTERVAL", 60)
    log_level = env.log_level("LOG_LEVEL", logging.DEBUG)

    mysql_host = env("MYSQL_HOST", '185.195.27.163')
    mysql_port = env.int("MYSQL_PORT", '3306')
    mysql_user = env("MYSQL_USER", 'sla')
    mysql_password = env("MYSQL_PASS", 'MySecurePass_123')
    mysql_db_name = env("MYSQL_DB_NAME", 'sla')

    kubernetes_master_node = env("KUBERNETES_MASTER_NODE", 'minikube')


class Mysql:
    def __init__(self, config: Config) -> None:
        logging.info('Connecting db')

        self.connection = mysql.connector.connect(host=config.mysql_host, user=config.mysql_user,
                                                  passwd=config.mysql_password, auth_plugin='mysql_native_password')

        self.tables = ['system_reliability', 'availability', "business_reliability"]

        logging.info('Starting migration')

        cursor = self.connection.cursor()
        for table in self.tables:
            cursor.execute('CREATE DATABASE IF NOT EXISTS %s' %
                           (config.mysql_db_name))

            cursor.execute('USE sla')

            cursor.execute(f"""
                CREATE TABLE IF NOT EXISTS {table}(
                    datetime datetime not null default NOW(),
                    name varchar(255) not null,
                    node varchar(255),
                    slo float(4) not null,
                    value float(4),
                    is_bad bool not null default false
                )""")

            indices = ["datetime", "name", "node"]

            for i in indices:
                cursor.execute(f"""SELECT COUNT(1) IndexIsThere FROM INFORMATION_SCHEMA.STATISTICS 
                    WHERE table_schema=DATABASE() AND table_name='{table}' AND index_name='{i}';""")
                index_exists = int(cursor.fetchall()[0][0])
                if not index_exists:
                    cursor.execute(f"""ALTER TABLE {table} ADD INDEX ({i})""")

    def save_indicator(self, table, name, node, slo, value, is_bad=False, time=None):
        cursor = self.connection.cursor()
        sql = f"INSERT INTO {table} (name, node, slo, value, is_bad, datetime) VALUES (%s, %s, %s, %s, %s, %s)"
        val = (name, node, slo, value, int(is_bad), time)
        cursor.execute(sql, val)
        self.connection.commit()

    def get_current_error_budget(self, table, node, hours):
        cursor = self.connection.cursor()
        cursor.execute(f"""
            SELECT (60 * {hours} - COUNT(DISTINCT datetime)) / (60 * {hours}) FROM {table}
            WHERE node='{node}'
            and datetime BETWEEN DATE_SUB(NOW(), INTERVAL {hours} HOUR) and NOW()
            and is_bad""")
        eb = float(cursor.fetchall()[0][0])
        return eb



class PrometheusRequest:

    def __init__(self, config: Config) -> None:  # add check that prom is accessible
        self.prometheus_api_url = config.prometheus_api_url

    def get_oncall_nodes(self, time):
        nodes = []
        response = requests.get(
            self.prometheus_api_url + '/api/v1/query', params={'query': "kube_node_info", 'time': time}
        )
        content = response.json()
        for e in content['data']['result']:
            nodes.append(e["metric"]["node"])

        return nodes

    def get_specified_val_for_each_node(self, default, time):
        ret_val = {}
        nodes = self.get_oncall_nodes(time)
        for node in nodes:
            ret_val[node] = default

        return ret_val

    def get_last_value_per_node(self, query, default, time):
        try:
            resp = requests.get(self.prometheus_api_url + '/api/v1/query', params={'query': query, 'time': time})
            content = resp.json()
            if not content or len(content['data']['result']) == 0:
                return self.get_specified_val_for_each_node(default, time)

            value_per_pod = self.get_specified_val_for_each_node(None, time)
            for result in content['data']['result']:
                value_per_pod[result["metric"]["node"]] = result["value"][1]
            return value_per_pod

        except Exception as error:
            logging.error(error, exc_info=True)
            return self.get_specified_val_for_each_node(default, time)

    def get_last_value(self, query, default, time):
        try:
            resp = requests.get(self.prometheus_api_url + '/api/v1/query', params={'query': query, 'time': time})
            content = resp.json()
            if not content or len(content['data']['result']) == 0:
                return default

            value = content['data']['result'][0]['value'][1]
            return value

        except Exception as error:
            logging.error(error, exc_info=True)
            return default, time


class SLI:
    def __init__(self, table, name, prom_query, slo, default_value, value_cast, is_bad_condition, multi_node=True):
        self.table = table
        self.name = name
        self.prom_query = prom_query
        self.slo = slo
        self.default_value = default_value
        self.value_cast = value_cast
        self.is_bad_condition = is_bad_condition
        self.mult_node = multi_node


class SLOChecker:
    def __init__(self, config):
        self.config = config
        self.db = Mysql(config)
        self.prom = PrometheusRequest(config)
        self.indicators = self.init_indicators(self.prom)


    def init_indicators(self, prom):
        timestamp = int(time.time())
        cpu_usage_limit = int(prom.get_last_value(
            query=f"kube_pod_container_resource_limits{{namespace='oncall', container='app-container', resource='cpu'}} * 1000",
            default=-1,
            time=timestamp
        ))
        mem_usage_limit = int(prom.get_last_value(
            query=f"kube_pod_container_resource_limits{{namespace='oncall', container='app-container', resource='memory'}} / 1024 /1024",
            default=-1,
            time=timestamp
        ))

        return [
            SLI(
                table="system_reliability",
                name="oncall_prober_create_user_scenario_failed_total",
                prom_query="increase(oncall_prober_create_user_scenario_failed_total[1m])",
                slo=0,
                default_value=100,
                value_cast=lambda x: int(float(x)),
                is_bad_condition=lambda x: x > 0
            ),
            SLI(
                table="system_reliability",
                name="oncall_prober_create_team_scenario_failed_total",
                prom_query="increase(oncall_prober_create_team_scenario_failed_total[1m])",
                slo=0,
                default_value=100,
                value_cast=lambda x: int(float(x)),
                is_bad_condition=lambda x: x > 0
            ),
            SLI(
                table="system_reliability",
                name="oncall_prober_create_event_scenario_failed_total",
                prom_query="increase(oncall_prober_create_event_scenario_failed_total[1m])",
                slo=0,
                default_value=100,
                value_cast=lambda x: int(float(x)),
                is_bad_condition=lambda x: x > 0
            ),
            SLI(
                table="system_reliability",
                name="oncall_prober_create_user_scenario_duration_seconds",
                prom_query="oncall_prober_create_user_scenario_duration_seconds",
                slo=0.1,
                default_value=2000,
                value_cast=lambda x: float(x),
                is_bad_condition=lambda x: x > 0.1
            ),
            SLI(
                table="system_reliability",
                name="oncall_prober_create_team_scenario_duration_seconds",
                prom_query="oncall_prober_create_team_scenario_duration_seconds",
                slo=0.2,
                default_value=2000,
                value_cast=lambda x: float(x),
                is_bad_condition=lambda x: x > 0.2
            ),
            SLI(
                table="system_reliability",
                name="oncall_prober_create_event_scenario_duration_seconds",
                prom_query="oncall_prober_create_event_scenario_duration_seconds",
                slo=0.1,
                default_value=2000,
                value_cast=lambda x: float(x),
                is_bad_condition=lambda x: x > 0.1
            ),
            SLI(
                table="system_reliability",
                name="oncall_exporter_scenario_health_status",
                prom_query="oncall_exporter_health_status",
                slo=1,
                default_value=0,
                value_cast=lambda x: int(float(x)),
                is_bad_condition=lambda x: x < 1
            ),
            SLI(
                table="system_reliability",
                name="oncall_exporter_scenario_failed_requests_ratio",
                prom_query="rate(oncall_exporter_api_requests_failed_total[1m]) / rate(oncall_exporter_api_requests_total[1m])",
                slo=0.1,
                default_value=1,
                value_cast=lambda x: float(x),
                is_bad_condition=lambda x: x > 0.1
            ),
            SLI(
                table="system_reliability",
                name="oncall_exporter_scenario_requests_duration_90%",
                prom_query="histogram_quantile(0.9,	rate(oncall_exporter_api_requests_duration_bucket[1m]))",
                slo=0.5,
                default_value=5,
                value_cast=lambda x: float(x),
                is_bad_condition=lambda x: x > 0.5
            ),
            SLI(
                table="system_reliability",
                name="oncall_cpu_usage",
                prom_query="rate(container_cpu_usage_seconds_total{namespace='oncall'}[1m]) * 1000",
                slo=cpu_usage_limit * 0.9,
                default_value=10000,
                value_cast=lambda x: float(x),
                is_bad_condition=lambda x: x > cpu_usage_limit * 0.9
            ),
            SLI(
                table="system_reliability",
                name="oncall_mem_usage",
                prom_query="container_memory_working_set_bytes{namespace='oncall'} /1024 /1024",  # Megabytes
                slo=mem_usage_limit * 0.9,
                default_value=10000,
                value_cast=lambda x: float(x),
                is_bad_condition=lambda x: x > mem_usage_limit * 0.9
            ),
            SLI(
                table="availability",
                name="bad_requests",
                prom_query="""(sum by (service) (rate(nginx_ingress_controller_requests{namespace="oncall"}[1m]))
                -
                sum by (service) (rate(nginx_ingress_controller_requests{namespace="oncall",status!="404",status!~"5.."}[1m])))
                /
                sum by (service) (rate(nginx_ingress_controller_requests{namespace="oncall"}[1m]))""",
                slo=0.05,
                default_value=0.9,
                value_cast=lambda x: float(x),
                is_bad_condition=lambda x: x > 0.05,
                multi_node=False
            ),
            SLI(
                table="availability",
                name="slow_requests",
                prom_query="""(sum by (service) (rate(nginx_ingress_controller_request_duration_seconds_count{namespace="oncall"}[1m]))
                -
                sum by (service) (rate(nginx_ingress_controller_request_duration_seconds_bucket{le="1", namespace="oncall"}[1m])))
                /
                sum by (service) (rate(nginx_ingress_controller_request_duration_seconds_count{namespace="oncall"}[1m]))""",
                slo=0.1,
                default_value=0.9,
                value_cast=lambda x: float(x),
                is_bad_condition=lambda x: x > 0.1,
                multi_node=False
            )
        ]

    def save_indicators(self, unixtimestamp, date_format):
        for sli in self.indicators:
            if sli.mult_node:
                values_per_node = self.prom.get_last_value_per_node(sli.prom_query, sli.default_value, unixtimestamp)
                for node, value in values_per_node.items():
                    is_bad = False
                    if not value is None:
                        value = sli.value_cast(value)
                        is_bad = sli.is_bad_condition(value)
                        if math.isnan(value):
                            value = 0

                    self.db.save_indicator(
                        table=sli.table, name=sli.name, node=node, slo=sli.slo, value=value,
                        is_bad=is_bad, time=date_format
                    )
                    logging.debug(f"saved '{sli.name}' value {value}")
            else:
                value = self.prom.get_last_value(sli.prom_query, sli.default_value, unixtimestamp)
                value = sli.value_cast(value)
                if math.isnan(value):
                    value = 0
                self.db.save_indicator(
                    table=sli.table, name=sli.name, node=None,
                    slo=sli.slo, value=value, is_bad=sli.is_bad_condition(value), time=date_format
                )
                logging.debug(f"saved '{sli.name}' value {value}")

    def save_error_budget(self, unixtimestamp, date_format):
        nodes = self.prom.get_oncall_nodes(unixtimestamp)
        total_sla = {"1h": 1, "6h": 1, "1d": 1, "7d": 1, "28d": 1}
        for node in nodes:
            for time_range, hours in {"1h": 1, "6h": 6, "1d": 24, "7d": 24 * 7, "28d": 24 * 28}.items():
                eb = self.db.get_current_error_budget('system_reliability', node, hours)
                name = f"oncall_current_error_budget_over_{time_range}"
                self.db.save_indicator(
                    table="system_reliability", name=name, node=node, slo=-1, value=eb, is_bad=False, time=date_format
                )
                if eb != 1:
                    total_sla[time_range] *= (1 - eb)

                logging.debug(f"saved '{name}' value {eb}")

        for time_range, eb in total_sla.items():
            name = f"oncall_current_total_error_budget_over_{time_range}"
            value = 1
            if eb != 1:
                value = 1 - eb
            self.db.save_indicator(
                table="system_reliability", name=name, node=None, slo=-1, value=value, is_bad=False, time=date_format
            )
            logging.debug(f"saved '{name}' value {value}")

    def run(self):
        logging.info(f"Starting sla checker")

        while True:
            logging.debug(f"Run prober")
            unixtimestamp = int(time.time())
            date_format = datetime.utcfromtimestamp(unixtimestamp).strftime('%Y-%m-%d %H:%M:%S')

            self.save_indicators(unixtimestamp, date_format)

            self.save_error_budget(unixtimestamp, date_format)

            logging.debug(f"Waiting {config.scrape_interval} seconds for next loop")
            time.sleep(config.scrape_interval)


def setup_logging(config: Config):
    logging.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%H:%M:%S',
                        level=config.log_level)
    logging.getLogger("urllib3").setLevel(logging.WARNING)


def terminate(signal, frame):
    print("Terminating")
    sys.exit(0)


if __name__ == "__main__":
    signal.signal(signal.SIGTERM, terminate)
    config = Config()
    setup_logging(config)
    checker = SLOChecker(config)
    checker.run()
