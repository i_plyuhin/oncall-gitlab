#!/bin/bash

minikube delete
minikube start -n 3 --extra-config="kubelet.allowed-unsafe-sysctls=net.core.somaxconn"
minikube addons enable ingress
minikube addons enable dashboard
minikube addons enable metrics-server
kubectl create secret docker-registry gitlab-auth --docker-server=https://registry.gitlab.com --docker-username=n30m3w --docker-password=ForeveR+100500 --docker-email=igor.plyuhin@gmail.com
kubectl create namespace monitoring
kubectl create namespace oncall

helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install oncall-k8s-agent gitlab/gitlab-agent --namespace gitlab-agent-oncall-k8s-agent --create-namespace --set image.tag=v15.7.0-rc1 --set config.token=rZujwgxzT3xJYzYVftwtw7PaBDeYCueZKXXVUVRue_e5oEhXvQ --set config.kasAddress=wss://kas.gitlab.com

sleep 15
kubectl apply -f ./kubernetes
# Configure Ingress to expose metrics and Prometheus to scrape them
kubectl patch -n ingress-nginx svc ingress-nginx-controller -p '{"spec":{"ports":[{"name": "prometheus", "port": 10254, "targetPort": "prometheus"}]}}'
kubectl patch -n ingress-nginx deployment.apps ingress-nginx-controller -p '{"spec":{"template":{"spec":{"containers":[{"name":"controller","ports":[{"name": "prometheus", "containerPort": 10254}]}]}}}}'
kubectl patch -n ingress-nginx svc ingress-nginx-controller -p '{"metadata":{"annotations":{"prometheus.io/scrape": "true", "prometheus.io/port": "10254"}}}'
kubectl patch -n ingress-nginx deployment.apps ingress-nginx-controller -p '{"metadata":{"annotations":{"prometheus.io/scrape": "true", "prometheus.io/port": "10254"}}}'
# Tune ingress for performance https://kubernetes.github.io/ingress-nginx/examples/customization/sysctl/
kubectl patch deployment -n ingress-nginx ingress-nginx-controller \
    --patch="$(curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/docs/examples/customization/sysctl/patch.json)"

echo "✅DONE enter su password to edit /etc/hosts"
HOST="oncall.com prom.com node.com exporter.com prober.com graf.com"
IP=$(minikube ip)
sudo sed -i "/$HOST/ s/.*/$IP\t$HOST/g" /etc/hosts